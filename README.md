# Automatic Speech Recognition and Personal Identifying Information masking

The aim of the project is to transcribe audio files containing conversations of clients with operators and to obscure the privacy elements of the conversation. 
The code takes as input the audio and gives as an output:
 - the original transcription
 - the censored transcription
 - the censored audio    

# Set-up [AWS ONLY]
Launch an AWS EC2 instance using the Deep Learning AMI (DLAMI) GPU PyTorch 1.13.1 (Amazon Linux 2) 20230201.
From the command line, activate the built-in environment in the DLAMI mounted on the EC2.
```
source activate pytorch
```

Set ffmpeg correctly
```
sudo su -
cd /usr/local/bin
mkdir ffmpeg 
cd ffmpeg
wget https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz
tar -xf ffmpeg-release-amd64-static.tar.xz
cd ffmpeg-X.Y.Z-amd64-static # X.Y.Z is the version of ffmpeg
mv ffmpeg /opt/conda/envs/pytorch/bin/ffmpeg # answer yes when prompted
exit
```

Check if setting ffmpeg has been installed. No errors should be raised.
```
ffmpeg -version
```

Assuming you are in the home directory, extract the files from the given archive using 'unzip' or 'tar'. You should now have a directory containing 'whisper' in its name. Assuming its name is whisper_speech_recognition, ```cd``` in it and install the needed Python modules.

```
cd whisper_speech_recognition
pip install -r requirements.txt --quiet
```

Download the needed models and check them. Paths in which the models have been downloaded will be written in ```paths_to_models.txt```. 
```
python download_models.py
python check_models.py 
```

If everything went well, the message "All models have been succesully downloaded" should be printed and you are ready to use the script.

# Command line usage

```
python multiple_audio_transcription_and_masking.py --audio_folder path/to/the/folder/containing/the/audios \
                                                   [--orig_trans_folder path/to/complete/transcription/folder] \
                                                   [--cens_trans_folder path/to/censored/transcription/folder] \
                                                   [--cens_audio_folder path/to/censored/audio/folder]
```

Notes
 - ```python multiple_audio_transcription_and_masking.py -h``` for the full list of arguments
 - The arguments `orig_trans_folder', `cens_trans_folder' and `cens_audio_folder' are set by default to the respective subfolders inside TEST/
 - The extension of the audio file is set as "mp3" by default. If you want to change it, for example, in "wav", add the '--audio_extension wav' argument
 - If you want to skip audios that already have a complete transcription in the specified folder, add the '--skip_transcription' argument

