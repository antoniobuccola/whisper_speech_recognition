import csv

def save_transcription(transcription, csv_name):
    
    with open(csv_name, 'w', newline='', encoding='utf-8') as csvfile:
    
        writer = csv.DictWriter(csvfile, fieldnames = list(transcription[0].keys()))
        writer.writeheader()
        
        for data in transcription:
            writer.writerow({'start': data['start'],
                             'end': data['end'],
                             'speaker': data['speaker'], 
                             'text': str(data['text'])})  