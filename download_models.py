import os
import subprocess
from huggingface_hub import snapshot_download
from pyannote.audio import Pipeline, Inference
from whisper import _download, _MODELS

# pyannote 
auth_token = "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"

seg_git_commit = "349a7a6b5ba030073f65ca2efe7940655cbc65cc"
path_seg = snapshot_download(repo_id = "pyannote/segmentation", revision = seg_git_commit, use_auth_token = auth_token)

vad_git_commit = "f3b1193d5288ed24de9d81b8b070d1d0482b6e68"
path_vad = snapshot_download(repo_id = "pyannote/voice-activity-detection", revision = vad_git_commit, use_auth_token = auth_token)
Pipeline.from_pretrained("pyannote/voice-activity-detection", use_auth_token = auth_token)
		   
emb_git_commit = "20b2db779562a3141f5eadd34a0232dbcd56d620" 
path_emb = snapshot_download(repo_id = "pyannote/embedding", revision = emb_git_commit, use_auth_token = auth_token)
Inference("pyannote/embedding", window = "whole", use_auth_token = auth_token)

# whisper (tiny, small, medium, large, large-v2)
whisper_size = "large-v2"
path_nlp = "nlp_models"
if not os.path.isdir(path_nlp):
    os.makedirs(path_nlp)

_download(_MODELS[whisper_size], root = path_nlp, in_memory = False)

# spacy (sm >> lg)
model = "it_core_news_lg"
url = f"https://github.com/explosion/spacy-models/releases/download/{model}-3.4.0/{model}-3.4.0.tar.gz"
subprocess.run(["wget", url, "-P", path_nlp + "/"])
subprocess.run(["tar", "-xf", f"{path_nlp}/{model}-3.4.0.tar.gz", "-C", path_nlp + "/"])

# opens a text file and write the directories in which the models have been downloaded

with open("paths_to_models.txt", "w") as f:
    
    print("Paths in which the models have been downloaded", file = f)
    
    print(f"Voice Activity Detection: {path_vad}", file = f)
    print(f"Segmentation: {path_seg}", file = f)
    print(f"Embedder: {path_emb}", file = f)
    print(f"Whisper ({whisper_size}): {path_nlp}", file = f)
    print(f"Spacy {model}: {path_nlp}", file = f)
    
    
