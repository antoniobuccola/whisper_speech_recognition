from spacy import load
from pandas import read_csv

#spacy_processor = load("nlp_models/it_core_news_sm-3.4.0/it_core_news_sm/it_core_news_sm-3.4.0")
spacy_processor = load("nlp_models/it_core_news_lg-3.4.0/it_core_news_lg/it_core_news_lg-3.4.0")

masked_pref = ["PER", "LOC", "NUM", "CF", "CAP", "POD", "PDR", "ID", "EMAIL", "DATE", "LF", "SP"]
masked_info = ["PERSON", "LOCATION", "NUMBER", "FISCAL CODE", "CAP", "POD CODE", "PDR CODE", "ID CARD NUMBER", "EMAIL", "DAY-MONTH-YEAR", "LETTER OF", "SPELLING"] 

converter = dict(zip(masked_pref, masked_info))

address_info = """calle, campiello, campo, carraia, carrarone, chiasso, circondario, circonvallazione, contrà, 
                  contrada, corso, diga, discesa, frazione, giardino, largo, località, lungoargine, lungolago, 
                  lungomare, maso, parallela, passeggiata, piazza, piazzale, piazzetta, rotonda, salita, strada, 
                  stradella, stradello, traversa, via, viale, vico, vicoletto, vicolo, vietta, viottolo, viuzza, viuzzo"""
                  
month_list = ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"] 

ignorables = ["euro", "centesimi", "giorni", "giorno", "rimborso", "rimborsi", "kW", "KW", "anno", "anni", "smc", "Smc", "giga"]                 

organizations = [
    
    "Italia", "Partita", "Edi", "Servizio", "Massima", "Enel", "Ene", "Supermoney", "Supermone", "Iliad", "Tim", "Wiliwi", "ERA", "Era", "Indirizzo", "Synergy", "Eni", "eni", "Eni Plenitude", "Plenitude", "Green Earth", "Green Network", "Green", "eolo", "Eolo", "Iren", "Sen", "Sev", "Wind3", "Iberdrola", "A2A", "Eracom", "Olimpia", "4G ENERGIA", "AB Energia", "Acea", "AGSM", "Antenore Energia", "Argos", "Ascotrade", "Atena", "Audax", "Autogas Nord", "Autogas", "Axpo", "Ayou Energia", "Barococ", "Beone", "Blu Meta", "Blue Energy", "bts gas", "Cogeser", "Di Ferco Energia", "Duferco", "Dolomiti Energia", "Domestika Energia", "E On", "Ecogas", "E-Distribuzione", "Egea", "Enegan", "Enercom", "Energia Comune", "Energit", "Engie", "Eon", "Estra", "Europe Srl", "Eviso", "Evolvere", "Exsenia", "Facile Energy", "Factory Energia", "Fintel", "Gelsia", "Gienna", "Grittigas", "Hera", "Illumia", "Italgas", "Luce e Gas Alperia", "Luce e Gas Sinergy", "Massima Energia", "Metano Nord", "New Energy", "Optima", "Patavium", "Phlogas and Power", "Pulser", "Revoluce", "S4 Energia", "Seigas", "Servizio Elettrico Nazionale", "Sidigas", "Sienergit", "Sinergas", "Sorgenia", "Tea Energia", "Union Energia", "Union Gas", "Uno Energy", "Vivigas", "Withu", "Xenia Energia", "Fastweb", "Irenlinkem", "Sky", "Tiscali"
    
    ]
    
with open("toolsanonimyzer/italian_names.txt", "r", encoding="utf-8") as file_names:
    people_names = [name.rstrip("\n") for name in file_names.readlines()]