import re
import librosa

import spacy
from spacy.tokens import Span
from spacy.lang.it import Italian

import numpy as np
import pandas as pd

from .constants import organizations, ignorables, address_info, masked_pref, masked_info, converter, people_names

def replace_location_words(text, address_info):
    
    replaced_text = text
    info = re.sub(" +", " ", address_info.replace("\n", ""))
    
    for s in info.split(", "):
        replaced_text = replaced_text.replace(f" {s}", f" {s.capitalize()}") 
        
    return replaced_text
    
def find_sensitive_info(text, nlp):
    
    pii_info = []
   
    doc = nlp(text)

    for e in doc.ents:
        
        texent = e.text
        
        # known organizations can be skipped
        if any(o in texent for o in organizations):
            continue
        
        label = e.label_
        pos = doc[e.start: e.end][0].pos_
        
        if (label in ["ORG", "MISC"]) or (pos in ["ADV", "VERB", "AUX"]):
            continue

        elif len(e) > 1:
            pii_info.append((texent, label))
            
        elif pos == "PROPN":
            pii_info.append((texent, label))
        
        else:
            continue
    
    emails = re.findall(r"\s?(\S*\.)?\s?(\S*\.[a-z]{2,8})\b", text)
    if len(emails) > 0:            
        for e in emails:
            email = e[0] + e[1]
            pii_info.append((email.lstrip(" "), "EMAIL"))  
    
    cleaned_text = text.replace(".", "")
    
    fiscal_codes = re.findall(r"\s?[A-Z]{3,6}\d{2}[A-Z]{1}\d{2}[A-Z]{1}\d{3}[A-Z]{1}", cleaned_text.replace("-", ""))
    if len(fiscal_codes) > 0:
        for f in fiscal_codes:
            pii_info.append((f.lstrip(" "), "CF"))
    
    pod_codes = re.findall(r"\s?I?T?\d{2,3}E\d{6,9}", cleaned_text)
    if len(pod_codes) > 0:
        for po in pod_codes:
            pii_info.append((po.lstrip(" "), "POD"))
        
    pdr_codes = re.findall(r"\s?\d{12,14}", cleaned_text)
    if len(pdr_codes) > 0:
        for pr in pdr_codes:
            pii_info.append((pr.lstrip(" "), "PDR"))
      
    card_ids = re.findall(r"\s?[A-Z ]{2,3}?[0-9 ]{5,20}", cleaned_text)
    if len(card_ids) > 0:
        for cid in card_ids:
            pii_info.append((cid, "ID")) 
    
    letters_of = re.findall(r"\s?[A-Z]{1} di", cleaned_text)
    if len(letters_of) > 0:
        for l in letters_of:
            pii_info.append((l, "LF"))
            
    spelling = re.findall(r"([A-Z][\- ])+[A-Z]?", cleaned_text)
    if len(spelling) > 0:
        for l in spelling:
            pii_info.append((l, "SP"))
     
    numbers = re.findall(r"(\s?\d|\-?\d|(\,\s?)\d)+", cleaned_text.replace(",", ""))
    if len(numbers) > 0:
        for n in numbers:   	    
        	pii_info.append((n.lstrip(" "), "NUM"))
                    
    pii_dict = {}
    
    for pii, l in pii_info:
        
        if len(pii) == 1:
            continue
        
        if l == "LOC" and len(pii.strip(" ")) < 3:
            continue
        
        if l == "NUM":
            pii = pii.strip(" ")
            
        if l == "PER":
            for sub_pii in pii.split():
                if len(sub_pii) >= 3 or sub_pii in people_names:
                    pii_dict[sub_pii] = f"<{converter[l]}>"
                    pii_dict[sub_pii.lower()] = f"<{converter[l]}>"
            
        pii_dict[pii] = f"<{converter[l]}>" 
        
    for name in people_names:
        if name in text:
            pii_dict[name] = "<PERSON>"
        
    pii_dict = dict(sorted(pii_dict.items(),key = lambda x : len(x[0]), reverse = True))
    
    return pii_dict
    
def mask_text(transcription_data, nlp, address_info):
    
    whole_original_text = " ".join([replace_location_words(d["text"], address_info) for d in transcription_data])
    whole_text = whole_original_text.replace("...", "").replace("?", "")
    #whole_text = replace_location_words(whole_text, address_info)
    
    pii_info = find_sensitive_info(whole_text, nlp)
    
    masked_data = []
    
    for r in transcription_data:

        sentence = r["text"]
        
        filtered_sentence = sentence
        
        for pii, info in pii_info.items():
            
            if pii in filtered_sentence:
                if info == "<NUMBER>":
                    
                    i = filtered_sentence.index(pii)
                    
                    if filtered_sentence[i-1] == "A" and filtered_sentence[i+1] == "A":
                        continue
                    
                    s = i-10
                    if s < 0:
                        s = 0
                        
                    e = i+20
                    if e > len(filtered_sentence):
                        e = len(filtered_sentence)
                    
                    a_slice = filtered_sentence[s : e]
                    if any(ign in a_slice for ign in ignorables):
            	        continue 
                    else:
                        filtered_sentence = filtered_sentence.replace(pii, info)   
                    
                elif info == "<EMAIL>":
                    
            	    i = filtered_sentence.index(pii)
            	     
            	    s = i-10
            	    if s < 0:
            	        s = 0
                     
            	    e = i+20
            	    if e > len(filtered_sentence):
            	        e = len(filtered_sentence)
            	        
            	    a_slice = filtered_sentence[s : e]
            	    filtered_sentence = filtered_sentence.replace(a_slice, info)    
                    
                else:
                    filtered_sentence = filtered_sentence.replace(pii, info)
                            
        filtered_sentence = re.sub(r"(<NUMBER>\s?)+", "<NUMBER>", filtered_sentence)
        filtered_sentence = re.sub(r"(<LOCATION>\s?)+", "<LOCATION>", filtered_sentence)
        
        masked_data.append({
                            "start": r["start"],
                            "end": r["end"],
                            "text": filtered_sentence,
                            "speaker": r["speaker"]
                        })
    
    return masked_data
    
def mask_audio(waveform, sr, masked_data):
    
    begin = masked_data[0]
    audio_list = [waveform[int(sr*begin["start"]) : int(sr*begin["end"])]]
    
    for r in masked_data[1:]:
        
        start = r["start"]
        end = r["end"]

        segment = waveform[int(sr*start) : int(sr*end)]
        
        if any(mask in r["text"] for mask in masked_info):
            
            t = end - start
            x = np.linspace(start = 0, stop = t, num = int(t*sr))
            doot = np.sin(3000*x)#*np.sin(int(t)*x)

            audio_list.append(doot)

        else:
            audio_list.append(segment)
            
    censored_audio = np.concatenate(audio_list).astype(np.float32) 
    return censored_audio 
