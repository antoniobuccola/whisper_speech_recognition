import re
import os
import pandas as pd

from spacy.tokens import Span
from spacy.matcher import Matcher
from spacy.lang.it import Italian

from .constants import month_list

@Italian.component("entity-fixer")
def entity_fixer(doc):
    
    new_spans = []
        
    for s in doc.sents:  
    
        for ent in s.ents:  
        
            tent = ent.text           
            
            if ("Iren" in tent or "Luce" in tent) and len(ent) == 1:
                
                iE = ent[0].i
                
                start = iE - 5
                if start < 0:
                    start = 0
                
                end = iE + 5
                if end >= len(s):
                    end = len(s)
                
                span = s[start : end]

                if any(t in span.text for t in ["chiamo", "nome", "Gas", "gas"]):
                    continue
                else:
                    span = Span(doc, ent.start, ent.end, label = "ORG")
                    if span not in new_spans:
                        new_spans.append(span)
                        
    doc.set_ents(new_spans, default = "unmodified")     

    return doc   

@Italian.factory("date-finder", default_config = {"setup": True})
def date_finder(nlp, name, setup: bool):
    
    months = "|".join(month_list)
    
    def DateFinder(doc):
        
        matcher = Matcher(nlp.vocab)
        patterns = [
                
                [{'LIKE_NUM': True}, {'TEXT': {"REGEX": f"^.*({months}).*$"}}, {"POS" : "ADP", "OP" : "?"}, {'LIKE_NUM': True}],
                [{'LIKE_NUM': True}, {'TEXT': {"REGEX": f"^.*({months}).*$"}}, {'LIKE_NUM': True}], 
                [{'LIKE_NUM': True}, {'TEXT': {"REGEX": f"^.*({months}).*$"}}],
                [{'TEXT': {"REGEX": "\d{2}\\.\d{2}\\.\d{4}"}}]
                
            ]
        
        matcher.add('dates', patterns, greedy = "LONGEST")
        
        matches = [Span(doc, m[1], m[2], label = "DATE") for m in matcher(doc)]
        doc.set_ents(matches, default = "unmodified")
        
        return doc
    
    return DateFinder
