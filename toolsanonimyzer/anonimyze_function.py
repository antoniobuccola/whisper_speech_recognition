import warnings

# This will filter out the specific warning occuring when loading an mp3 file
warnings.filterwarnings("ignore", category = UserWarning, message = "PySoundFile failed. Trying audioread instead.")

import librosa
import resemblyzer

from .Anonimyzer import mask_text, mask_audio
from .constants import address_info
from .spacy_components import entity_fixer, date_finder

def mask_audio_and_text(audio_path, transcription_data, spacy_processor):
    
    # text masking
    if "date_finder" not in spacy_processor.pipe_names:
        spacy_processor.add_pipe("date-finder", name = "date_finder", before = "ner")
                                 
    if "entity_fixer" not in spacy_processor.pipe_names:
        spacy_processor.add_pipe("entity-fixer", name = "entity_fixer", before = "ner") 
        
    # transcription data format
    # a list of dictionaries associated to each turn where the keys are start, end, speaker, text
    # start / end : where the turn in the original audio file starts / ends
    # speaker : the one who spoke
    # text : the transcription of the corresponding turn
        
    masked_transcription = mask_text(transcription_data, spacy_processor, address_info)
    
    # audio masking
    sr = 16000
    waveform = resemblyzer.preprocess_wav(audio_path, source_sr = sr)
    masked_waveform = mask_audio(waveform, sr = sr, masked_data = masked_transcription)
    
    return masked_waveform, masked_transcription