from pandas import read_csv

from pyannote.audio import Pipeline, Inference

from spectralcluster import SpectralClusterer, LaplacianType, RefinementOptions, ThresholdType
from spectralcluster import ICASSP2018_REFINEMENT_SEQUENCE

# Voice Activity Detection & Audio Embedding

auth_token = "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" 

pipeline = Pipeline.from_pretrained("pyannote/voice-activity-detection", use_auth_token = auth_token)
embedder = Inference(f"pyannote/embedding", window = "whole", use_auth_token = auth_token)

# Speaker Identification

refinement_opts = RefinementOptions(gaussian_blur_sigma = 0.5,
                                    p_percentile = 0.95,
                                    thresholding_soft_multiplier = 0.20,
                                    thresholding_type = ThresholdType.RowMax,
                                    refinement_sequence = ICASSP2018_REFINEMENT_SEQUENCE)

clusterer = SpectralClusterer(min_clusters = 2, 
                              max_clusters = 2,
                              laplacian_type = LaplacianType.GraphCut, 
                              refinement_options = refinement_opts, 
                              custom_dist = "mahalanobis")
                              
operator_notes = [
    
    "numero cliente", "codice cliente", "dica", "aiut", "Supermone", "rispondo", "Italia", "Bulgaria", "essere utile", 
    "esserle utile", "posso aiutar"

    ]

customer_notes = [
    
    "cliente", "chiam", "informazion", "ultima bolletta", "contratto", "problem", "nuova registrazione"
    
    ]

connections = {
     
    "A2A Energia": ["due ad energia"],
    "Atena": ["Chiatena", "a Sena", "Athena", "la Tena"],
    "Bulgaria" : ["bugiaria"],
    "Contattiamo" : ["Contacciamo"],
    "Edison": ["di sonno", "E di sonno", "Davison", "Eddie", "edison", "Ledison", "Reddison", "Edisonne"], 
    "Enel": ["E nel", "Ener", "E-energia", "NRZ", "lei ne", "lei nel"],
    "Enel energia": ["nell'energia", "È nell'energia", "è nell'energia", "NLG", "lei la energia", "n-energia", "Eneleenergia", "E nell'energia", "enne all'energia", " e l'energia", "Lei è nell'energia", "lei è nell'energia", "Lei è in energia", "lei è in energia", "NLNergia"],
    "Energia": ["Enelgia", "Enelgy"],                      
    "Eni": ["Ehi", "IENI", "Ieni", "Aini", "Nisi", "EMEA", "EMI", "È il mio", "Enia", "Denis", "Leni", "ieni", "Eli", "Eny", "l'enni", "Enix","l'elito", "Lenny", "Amy", "Emi", "eleni", "Any"],
    "Estra": ["Escra"],
    "IBAN": ["IDON", "Ivan", "Ibana", "Liban", "Iban"],   
    "Iberdrola": ["Icergroda", "Iberidola", "iber droga", "iperdrola", "iberdrola"],
    "Illumia": ["il Lumia"],
    "Iren": ["Irene", "Irem", "IRM", "Aire", "aire", "AIRE", "Ren", "Irena", "Linen", "Irenna"],
    "Italia": ["Iceaia"],
    "Mercato Libero": ["Estato Libro"],
    "PDR": ["TDR", "Gierra", "PBR", "che gli è", "TGR", "PGR", "POS", "TBR", "di gr", "di diere", "PTR", "TTR", "CDR"],
    "POD": ["TOD", "forno", "POZ", "Pote ", "FOD", "podi", "COD", "Pod ", "podo"],
    "Supermoney": ["Supermoni", "superman", "Superman", "Supermonetro", "supermani", "Supermoli", "Cupermone", "SuperMoney", "Supermonero", "supermanico", "Supermoglia", "Supermoneda", "Supermoon", "Supermaria"],
    "addebito": ["attepito"],
    "adeguamenti": ["adesuamenti"],
    "affitto": ["acciuto", "assitto "],   
    "allaccio": ["allascio", "all'accio", "allasso", "all'asto"],
    "anno scorso": ["hanno scosso"],
    "approvazione": ["attrovazione"],
    "attivazione": ["accodazione"],
    "bolletta": ["bolleccia", "buonetta", "polletta", "bolletto", "boletta", "bolessa"],
    "bollette": ["bollecce", "volente", "buonette", "bolletti", "volete", "volette", "bolette"],
    "cambio di intestazione": ["cambi in testazione"],
    "carta": ["corsa"],
    "centesimi": ["litesimi"],  
    "chiusura": ["cittura", "gestura"], 
    "cliente": ["clienta"],
    "codice fiscale": ["colisso ufficiale", "con il dischi a fissare", "Codice di sale", "codice cale"],
    "codice POD": ["connessore", "colittacodo"],
    "contatore": ["montatore", "contattore", "condatore", "contattatore", "coltacolo", "dottore"],
    "contattiamo": ["contacciamo"],
    "contattato": ["contaccato"],
    "contratto": ["contrasto", "centracchio"],
    "contratti": ["contrasti"],
    "controllerò": ["controlerò"],
    "dati": ["d'atti"],
    "dettare": ["destare"],
    "dimezza": ["di mezza"],
    "fattura": ["facciuda", "frattura", "fatura", "cattura", "fastura"],
    "fiscale": ["T-Scala", "sitale", "pistale", "di scala", "picciole", "di scelte", "di tale", "PISTALER"],
    "fornitore": ["connettore", "fornitorie", "fotore"],
    "fornitura": ["formica"],
    "gas": ["garso", "gatto", "Lazzo", "lazzo", "dioss", "casta", "rigatti", "gassio", "gasto", "Gasta", "gioco"],
    "gas naturale": ["gastronaturale"],
    "gli sconti": ["disconti"],
    "insolvente": ["in scolvente"],
    "intestare": ["infestare", "ingessare"],
    "intestate": ["incestate"],
    "intestato": ["infestato"],
    "ko": ["cappotte"],
    "l'appalto": ["la falto"],
    "libero": ["libro"],
    "luce": ["Lucci", "lusco", "luci", "l'us"],
    "luce e gas": ["Lucchegaz", "gaseluce", "Duce di Ossi", "l'ussegasta", "Lucio Gaffs", "Michel Garde", "Gatteluzze", "Cattoluce", "Luke ed Asso", "all'us e gas", "Lucegas", "luce e gassa", "Lucio Gas", "Luzia e Gas"],
    "lunedì": ["l'umedì"],
    "maggio": ["mangio"],  
    "mensile": ["mentile"],
    "metano" : ["meccano", "Mecano"],
    "procedura": ["fissura"],
    "provincia": ["province"],
    "richiesta": ["rigghezza", "rischiata"],
    "soltanto": ["sul stand"], 
    "subentro": ["subendro", "giubbentro", "tubendo", "sub entro", "sub-bank"],
    "subentri": ["subendri"],
    "tariffa": ["carissa", "caniccia"],
    "tariffe": ["cariste", "carizone"],
    "voltura": ["voitura", "volveta", "postura", "volcura", "votura", "voltila", "cultura", "volata", "volsura", "coltura"],    
}

replacements = {rep : ok for ok, replaces in connections.items() for rep in sorted(replaces, key = lambda rep: len(rep), reverse = True)}
