import warnings

# This will filter out the specific warning occuring when loading an mp3 file
warnings.filterwarnings("ignore", category = UserWarning, message = "PySoundFile failed. Trying audioread instead.")

import librosa
import resemblyzer 

from .CallProcessor import get_speaker_diarization
from .CallTranscriber import get_raw_transcription_results, get_timestamp_aligned_transcription, replace_words, assign_speaker

from .constants import replacements

def transcribe_audio(audio_path, audio_duration, spoken_duration, whisper_model):

    sr = 16000
    res_waveform = resemblyzer.preprocess_wav(audio_path, source_sr = sr)
    if not audio_duration is None:
        waveform = res_waveform[:int(audio_duration * sr)]
    else:
        waveform = res_waveform
    
    diarization_data = get_speaker_diarization(waveform, sr, spoken_duration = spoken_duration)
    
    # transcription data format
    # a list of dictionaries associated to each turn where the keys are start, end, speaker, text
    # start / end : where the turn in the original audio file starts / ends
    # speaker : the one who spoke
    # text : the transcription of the corresponding turn
    
    raw_whisper_data = get_raw_transcription_results(whisper_model, waveform, language = "it")
    aligned_data = get_timestamp_aligned_transcription(diarization_data, raw_whisper_data) 
    replaced_data = replace_words(aligned_data, replacements)
    whisper_data = assign_speaker(replaced_data)
    
    return whisper_data