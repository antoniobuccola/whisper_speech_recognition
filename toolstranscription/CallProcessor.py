import os
import librosa

import numpy as np
import pandas as pd
import soundfile as sf
import noisereduce as nr

from time import time
from pyannote.core import Segment

from .constants import pipeline, embedder, clusterer, refinement_opts

###########################

def get_speaker_diarization(waveform, sr, spoken_duration = None):

    filt_audio = nr.reduce_noise(y = waveform, sr = sr)
    
    filt_name = "filtered_audio.wav"
    sf.write(filt_name, data = filt_audio, samplerate = sr)
    
    vad = pipeline(filt_name)
    
    starts = []
    ends = []
    durations = []
    
    segments = []
    embeddings = []
    
    for speech in vad.get_timeline().support():
    
        start = speech.start
        end = speech.end
        
        start_sample = int(start*sr)
        end_sample = int(end*sr)
        
        # slicing according to the samples of numpy array 
        segment = filt_audio[start_sample : end_sample] 
        if len(segment) == 0:
            continue
            
        orig_segment = waveform[start_sample : end_sample] 
        
        excerpt = Segment(start, end)
        
        try:
            embedding = embedder.crop(filt_name, excerpt)
        except RuntimeError:
            continue
        
        if np.isnan(embedding).sum() > 0:
            continue
            
        starts.append(start)
        ends.append(end)
        durations.append(end - start)
            
        embeddings.append(embedding)
        segments.append(orig_segment)
                
        if spoken_duration is not None:
            
            current_spoken_wave = np.concatenate(orig_segments)
            current_spoken_duration = len(current_spoken_wave) / sr
            
            if current_spoken_duration > int(spoken_duration):
                break
    
    labels = clusterer.predict(np.array(embeddings))
    timelines = np.cumsum(durations).tolist()
    
    diarized_data = [
        
        {"segment": segment, "start": start, "end": end, "label": label, "timeline": timeline} 
        for segment, start, end, label, timeline in zip(segments, starts, ends, labels, timelines)
    ]
    
    os.remove(filt_name)
    
    return diarized_data

def get_lofi_batches(diarized_data, sr, single_segment_duration = 240):

    if "segment" not in diarized_data[0]:
        raise KeyError(f"The provided diarized data after speaker diarization does not contain 'segment' key")
    
    lofi_track = np.concatenate([d["segment"] for d in diarized_data])
    
    duration = len(lofi_track) / sr
    N = int(duration / single_segment_duration) + 1
    
    batches = np.array_split(lofi_track, N)
    
    return batches
    
def bandwith_extension(lofi_file):
    
    current_working_dir = os.getcwd()
    
    os.chdir("CaW")
    
    name = lofi_file.split('/')[-1]
    
    os.system(f"python extend.py --input_folder VCTK_p347_363_to_371 --lr_signal {name}")
    
    bw_ext_file = f"./outputs/VCTK_p347_363_to_371/GeneratedSignals/{name[:-4]}_extended.wav"
    bw_audio_segment = librosa.load(bw_ext_file, sr = 16000)[0]
    
    os.remove(bw_ext_file)
    os.chdir(current_working_dir)
    
    return bw_audio_segment

def get_bandwidth_extended_track(audio_batches, sr, verbose = False):          

    track_speaker = np.array([])
    
    if verbose:
        start_ = time()
    
    lofi_file = "./CaW/inputs/lofi_waveform.wav"
    
    for i, piece in enumerate(audio_batches):
        
        if verbose:
            print(f"processing batch {i+1} / {len(audio_batches)} time {round(time() - start_, 3)}")
            
        sf.write(lofi_file, piece, samplerate = sr)
        
        try:
            track_piece = bandwith_extension(lofi_file)
            track_speaker = np.concatenate([track_speaker, track_piece])
    
        except Exception as E:
            if verbose:
                
                print()
                print(f"xxxx batch {i+1} / {len(audio_batch)} of {name} time {round(time() - start_, 3)}")
                print(E)
                print()
                
            os.chdir("../")
            pass
        
        os.remove(lofi_file)
    
    return track_speaker.astype(np.float32)                       
