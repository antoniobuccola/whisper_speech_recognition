import pandas as pd
from .constants import operator_notes, customer_notes
    
def get_raw_transcription_results(whisper_model, track, language = "it"):
    
    results = whisper_model.transcribe(track, language = language)  
    
    segments = [{"start": segment["start"], "end": segment["end"], "text": segment["text"]} for segment in results["segments"]]
    
    return segments
    
def assign_label(row, intervals_0_label, intervals_1_label) -> int:

    if any((row["start"] >= I[0] and row["end"] <= I[1]) for I in intervals_0_label):
        return 0

    elif any((row["start"] >= I[0] and row["end"] <= I[1]) for I in intervals_1_label):
        return 1
        
    else:
        return -1    

def get_timestamp_aligned_transcription(speaker_diarized_data, whisper_data): 
    
    # reference timeline
    tl_ref = speaker_diarized_data.copy()
    
    # to-be-aligned timeline
    tl_ali = whisper_data.copy()
    
    # reference timeline does not start with t = 0
    # yet it is the "correct" one to align the transcriptions   
    
    t0 = tl_ref[0]["start"]
    tl_ref = [{"start": d["start"] - t0, "end": d["end"] - t0, "label": d["label"], "timeline": d["timeline"]} for d in tl_ref]
    
    # now both timelines start at t = 0

    alignement = {0 : [], 1 : []}
    
    last_end_ali = 0
    
    for row_ref in tl_ref:

        best_dt = 1000

        end_ali = None
        label = None

        for row_ali in tl_ali:

            dt = abs(row_ref["timeline"] - row_ali["end"])
            
            if dt < best_dt:

                best_dt = dt
                end_ali = row_ali["end"]
                label = row_ref["label"]

        if last_end_ali != end_ali:
            alignement[label].append((last_end_ali, end_ali))

        last_end_ali = end_ali
    
    raw_aligned_data = [{"start": d["start"], "end": d["end"], "label": assign_label(d, alignement[0], alignement[1]), "text": d["text"]} for d in whisper_data]
    
    aligned_df = [d for d in raw_aligned_data if d["label"] >= 0]
    
    return aligned_df
    
    
#######

from pprint import pprint

def replace_words(data, replacements):
    
    replaced_data = []
    
    for d in data:

        repl = d.copy()
        text = d["text"]
        
        for orig, replacer in replacements.items():        
            text = text.replace(orig, replacer)
        
        repl["text"] = text
        replaced_data.append(repl)
    
    return replaced_data

def assign_speaker(data):
    
    operator_label = None
    customer_label = None

    for r in data:

        if any(so in r["text"] and sc not in r["text"] for sc in customer_notes for so in operator_notes):    
            
            operator_label = r["label"]
            customer_label = 1 - r["label"]
            break
        
        if any(sc in r["text"] and so not in r["text"] for sc in customer_notes for so in operator_notes):
            
            operator_label = 1 - r["label"] 
            customer_label = r["label"]
            break

    if customer_label is None:
        naming = {0 : "Speaker A", 1 : "Speaker B"}
    else:
        naming = {customer_label : "Customer", operator_label : "Call center operator"}
    
    speaker_assigned_data = [{"start": r["start"], "end": r["end"], "speaker": naming[r["label"]], "text": r["text"]} for r in data]
    
    return speaker_assigned_data       