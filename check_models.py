import traceback as tb
from sys import exit
import whisper
from spacy import load
from pyannote.audio import Pipeline, Inference

print()
print("Checking whisper model")
try:
    #whisper.load_model("tiny", download_root = "./nlp_models")
    whisper.load_model("large-v2", download_root = "./nlp_models")
except Exception as E:
    print("During loading whisper model occurred this exception")
    tb.print_exc()
    exit()
    
auth_token = "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"    

print()
print("Checking pyannote embedding")
try:
    Inference("pyannote/embedding", window = "whole", use_auth_token = auth_token)
except Exception as E:
    print("During loading pyannote embedding model occurred this exception")
    tb.print_exc()
    exit()
    
print()
print("Checking pyannote VAD")
try:
    Pipeline.from_pretrained("pyannote/voice-activity-detection", use_auth_token = auth_token)
except Exception as E:
    print("During loading pyannote VAD model occurred this exception")
    tb.print_exc()
    exit()
    
print()
print("Checking spacy NLP")

try:
    #load("./nlp_models/it_core_news_sm-3.4.0/it_core_news_sm/it_core_news_sm-3.4.0")
    load("./nlp_models/it_core_news_lg-3.4.0/it_core_news_lg/it_core_news_lg-3.4.0")
except Exception as E:
    print("During loading spacy model occurred this exception")
    tb.print_exc()
    exit()
    
print("All models have been succesully downloaded")

