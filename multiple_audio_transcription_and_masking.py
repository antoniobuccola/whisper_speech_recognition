import traceback as tb
import warnings

# This will filter out the specific warning occuring when loading an mp3 file
warnings.filterwarnings("ignore", category = UserWarning, message = "PySoundFile failed. Trying audioread instead.")

import os
import re
import csv
import sys
import librosa
import whisper

import soundfile as sf

from glob import glob
from pydub import AudioSegment

from time import time

from toolstranscription.transcription_function import transcribe_audio
from toolsanonimyzer.anonimyze_function import mask_audio_and_text

from toolstranscription.constants import replacements
from toolsanonimyzer.constants import spacy_processor
from toolsanonimyzer.spacy_components import entity_fixer, date_finder

sys.path.append("python")
from utilities import save_transcription

import argparse 

parser = argparse.ArgumentParser(description = """Diarization, Transcription & Masking - multiple files""", epilog = "See you later")

parser.add_argument("--audio_folder", required = True, help = "Folder in which the audios are stored")

parser.add_argument("--audio_extension", help = "Extension of the audios to be analyzed (default: mp3)", default = "mp3", type = str)
parser.add_argument('--audio_duration', default = None, help = "Limit the duration of the processed audios (seconds, default: no limit)")
parser.add_argument('--spoken_duration', default = None, help = "Limit the duration of the spoken-only audios (seconds, default: no limit)")

parser.add_argument('--skip_transcription', dest = 'skip_transcription', action = 'store_true', default = False,
                    help = "If not activated, it will overwrite the original transcription even if already existing in the specified folder")

parser.add_argument('--skip_censoring', dest = 'skip_censoring', action = 'store_true', default = False,
                    help = "If not activated, it will overwrite the censored transcription even if already existing in the specified folder")                    
                    
parser.add_argument('--transcription_model_path', default = "./nlp_models", 
                    help = "Path to Whisper transcription model (default: ./nlp_models")

parser.add_argument('--orig_trans_folder', default = "TEST/transcriptions",
                    help = "Folder to original transcription (default: TEST/transcriptions)")
                    
parser.add_argument('--cens_trans_folder', default = "TEST/censored_transcriptions",
                    help = "Folder to censored transcription (default: TEST/censored_transcriptions)")
                    
parser.add_argument('--cens_audio_folder', default = "TEST/censored_audios",
                    help = "Folder to censored audio (default: TEST/censored_audios)")

args = parser.parse_args()

audio_folder = args.audio_folder
if not os.path.exists(audio_folder):
    raise FileNotFoundError(f"{audio_folder} folder does not exist")
    
ext = args.audio_extension
audios = [a for a in glob(f"{audio_folder}/*.{ext}") if "filt" not in a]

orig_trans_folder = args.orig_trans_folder
if not os.path.exists(orig_trans_folder):
    raise FileNotFoundError(f"{orig_trans_folder} folder does not exist")

cens_trans_folder = args.cens_trans_folder
if not os.path.exists(cens_trans_folder):
    raise FileNotFoundError(f"{cens_trans_folder} folder does not exist")
    
cens_audio_folder = args.cens_audio_folder  
if not os.path.exists(cens_audio_folder):
    raise FileNotFoundError(f"{cens_audio_folder} folder does not exist")

whisper_model = whisper.load_model("large-v2", download_root = args.transcription_model_path)
    
if "date_finder" not in spacy_processor.pipe_names:
    spacy_processor.add_pipe("date-finder", name = "date_finder", before = "ner")
                             
if "entity_fixer" not in spacy_processor.pipe_names:
    spacy_processor.add_pipe("entity-fixer", name = "entity_fixer", before = "ner") 
    
skip_transcription = args.skip_transcription   
skip_censoring = args.skip_censoring 
start_ = time()
    
for audio_path in audios:
    
    print("Now processing", audio_path.split("/")[-1], "- Time:", round(time() - start_))
    
    try:
        file_name = re.search("RE[a-zA-Z0-9]+", audio_path).group()
        
        orig_transcription_name = f"{orig_trans_folder}/original_transcription_{file_name}.csv"
        if os.path.exists(orig_transcription_name) and skip_transcription:
            
            print("*"*10, orig_transcription_name, "already exists.")
            
            # read the transcription, without masked personal identifying information (PII). It will be used later for anonimyzation
            with open(orig_transcription_name, 'r', encoding = "utf-8") as csvfile:
                reader = csv.DictReader(csvfile)
                transcription_data = [{"start": float(row['start']), "end": float(row['end']), "speaker": row['speaker'], "text": row['text']} for row in reader]
        else:
            # transcription of the audio file
            transcription_data = transcribe_audio(audio_path, args.audio_duration, args.spoken_duration, whisper_model)
            
            # save the results of the transcription as a csv file
            save_transcription(transcription_data, orig_transcription_name)
         
        cens_transcription_name = f"{cens_trans_folder}/censored_transcription_{file_name}.csv"
        if os.path.exists(cens_transcription_name) and skip_censoring:
            print("*"*10, cens_transcription_name, "already exists.")
        
        else:
            # mask the PII in audio file and transcription   
            masked_waveform, masked_transcription = mask_audio_and_text(audio_path, transcription_data, spacy_processor)
            
            # save the results of the censored transcription as a csv file
            save_transcription(masked_transcription, cens_transcription_name)
            
            # save the censored audio as mp3
            censored_audio_name = f"{cens_audio_folder}/censored_audio_{file_name}.wav"
            sf.write(censored_audio_name, masked_waveform, samplerate = 16000)
            AudioSegment.from_wav(censored_audio_name).export(censored_audio_name.replace("wav", ext), format = ext)
            os.remove(censored_audio_name)
        
    except Exception as E:
        
        # if something wrong occurs, skip the audio and go to the next one
        print()
        print("During processing of file generated this error")
        tb.print_exc()

print("Finish - Time:", round(time() - start_))
